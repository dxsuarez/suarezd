//
//  Item.swift
//  examen
//
//  Created by Diego Suárez on 6/6/18.
//  Copyright © 2018 Diego Suárez. All rights reserved.
//

import Foundation

struct ItemInfo: Decodable {
    let data: [Item]
}

struct Item: Decodable {
    let label:String
    let value:Int
}
