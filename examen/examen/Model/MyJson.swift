//
//  MyJson.swift
//  examen
//
//  Created by Diego Suárez on 6/6/18.
//  Copyright © 2018 Diego Suárez. All rights reserved.
//

struct MyJsonInfo: Decodable {
    let viewTitle:String
    let date:String
    let nextLink:String
}
