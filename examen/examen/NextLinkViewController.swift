//
//  NextLinkViewController.swift
//  examen
//
//  Created by Diego Suárez on 6/6/18.
//  Copyright © 2018 Diego Suárez. All rights reserved.
//

import UIKit

class NextLinkViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var itemsTableView: UITableView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var averageLabel: UILabel!
    
    var nextLink:String?
    var name:String?
    var items:[Item] = []
    var average:Float = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabel.text = "Hi $(" + name! + ")"
        
        let urlString =  "https://api.myjson.com/bins/182sje"
        let url = URL(string: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: url!) {
            data, response, error in
            
            guard let data = data else {
                print("Error NO data")
                return
            }
            
            guard let itemInfo = try? JSONDecoder().decode(ItemInfo.self, from: data) else {
                print("Error decoding")
                return
            }

            DispatchQueue.main.async {
                
                self.items = itemInfo.data
                var total:Int = 0
                
                self.items.forEach { (item:Item) in
                    total += item.value
                }
        
                self.average = Float(total) / Float(self.items.count)
                self.itemsTableView.reloadData()
                self.averageLabel.text = "Average: " + String(self.average)
            }
        }
        
        task.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        itemsTableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell") as! ItemTableViewCell
        let item = self.items[indexPath.row]
        let label = item.label
        let value = String(item.value)
        
        cell.labelLabel.text = label
        cell.valueLabel.text = value
        
        return cell
    }
}
