//
//  ViewController.swift
//  examen
//
//  Created by Diego Suárez on 6/6/18.
//  Copyright © 2018 Diego Suárez. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var nextLink: String?
    
    @IBOutlet weak var viewTitleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let urlString =  "https://api.myjson.com/bins/72936"
        let url = URL(string: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: url!) {
            data, response, error in
            
            guard let data = data else {
                print("Error NO data")
                return
            }
            
            guard let myJsonInfo = try? JSONDecoder().decode(MyJsonInfo.self, from: data) else {
                print("Error decoding")
                return
            }
            
            DispatchQueue.main.async {
                self.viewTitleLabel.text = "\(myJsonInfo.viewTitle)"
                self.dateLabel.text = "\(myJsonInfo.date)"
                self.nextLink = "\(myJsonInfo.nextLink)"
            }
        }
        
        task.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let name = nameTextField.text
        let destination = segue.destination as! NextLinkViewController;
        destination.name = name
        destination.nextLink = nextLink
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "nextLinkSegue", sender: self)
    }
}
